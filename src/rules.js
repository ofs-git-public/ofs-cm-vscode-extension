/* eslint-disable @typescript-eslint/naming-convention */
'use strict';
exports.__esModule = true;
exports.Rules = void 0;
var vscodeInstance = require("vscode");
var Rules = /** @class */ (function () {
    function Rules() {
    }
    Rules.multilineEnterRules = [
        {
            // e.g. /** | */
            beforeText: /^\s*\/\*\*(?!\/)([^\*]|\*(?!\/))*$/,
            afterText: /^\s*\*\/$/,
            action: { indentAction: vscodeInstance.IndentAction.IndentOutdent, appendText: ' * ' }
        }, {
            // e.g. /** ...|
            beforeText: /^\s*\/\*\*(?!\/)([^\*]|\*(?!\/))*$/,
            action: { indentAction: vscodeInstance.IndentAction.None, appendText: ' * ' }
        }, {
            // e.g. /*! | */
            beforeText: /^\s*\/\*\!(?!\/)([^\*]|\*(?!\/))*$/,
            afterText: /^\s*\*\/$/,
            action: { indentAction: vscodeInstance.IndentAction.IndentOutdent, appendText: ' * ' }
        }, {
            // e.g.  * ...|
            beforeText: /^(\t|(\ ))*\ \*(\ ([^\*]|\*(?!\/))*)?$/,
            action: { indentAction: vscodeInstance.IndentAction.None, appendText: '* ' }
        }, {
            // e.g.  */|
            beforeText: /^(\t|(\ ))*\ \*\/\s*$/,
            action: { indentAction: vscodeInstance.IndentAction.None, removeText: 1 }
        }
    ];
    Rules.hashEnterRules = [
        {
            // e.g. # ...|
            beforeText: /^\s*#/,
            action: { indentAction: vscodeInstance.IndentAction.None, appendText: '# ' }
        }
    ];

    Rules.semicolonEnterRules = [
        {
            // e.g. ; ...|
            beforeText: /^\s*;/,
            action: { indentAction: vscodeInstance.IndentAction.None, appendText: '; ' }
        }
    ];

    Rules.endCommentEnterRules = [
        {
            // need to remove an extra space that is added after the end of a comment. 
            beforeText: /\*\//,
            action: {indentAction: vscodeInstance.IndentAction.Outdent}
        }
    ];
    return Rules;
}());
exports.Rules = Rules;
